odoo.define('cssa.controller', function (require) {
"use strict";

var core = require('web.core');
var ListController = require('web.ListController');
var rpc = require('web.rpc');
var session = require('web.session');
var _t = core._t;

ListController.include({
   renderButtons: function($node) {
       this._super.apply(this, arguments);
           if (this.$buttons) {
             this.$buttons.find('.find_zones').click(this.proxy('_onFindZones')) ;
           }
       },

       _onFindZones() {
           return this._rpc({
               model: 'cssa.zones',
               method: 'find_zones',
               args: ['id'],
           }).then(function(result) {
               //location.reload()
           })
       }

   })
});