# -*- coding: utf-8 -*-

from odoo import models, fields, api

class MailActivity(models.Model):
    _inherit = 'mail.activity'

    orario = fields.Char('Orario')

    user_id = fields.Many2one(
        'res.users', 'Assigned to',
        default=lambda self: self.env.user,
        index=True, required=True)

    @api.onchange('orario')
    def _onchange_orario(self):
        opportunity = self.env['crm.lead'].search([('id','=',self.res_id)])
        opportunity.orario = self.orario
