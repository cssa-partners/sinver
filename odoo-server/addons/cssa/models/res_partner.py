# -*- coding: utf-8 -*-

from odoo import models, fields, api
import urllib.parse


class Partner(models.Model):
    _inherit = 'res.partner'

    pratica_legale = fields.Selection([
        ('Inviati documenti','Inviati documenti'),
        ('Decreto ingiuntivo','Decreto ingiuntivo'),
        ('Precetto','Precetto'),
        ('Definizione pratica','Definizione pratica')
    ], 'Pratica Legale')

    relationship = fields.Selection([
        ('Cliente','Cliente'),
        ('Fornitore','Fornitore')
    ], 'Rapporto commerciale')

    pratica_legale_data = fields.Date('Data pratica')

    posizione_lavorativa = fields.Selection([
        ('CEO - SRL/SPA','CEO - SRL/SPA'),
        ('AMMINISTRATORE UNICO - SRL/SPA','AMMINISTRATORE UNICO - SRL/SPA'),
        ('SOCIO ACCOMANDATARIO - SAS','SOCIO ACCOMANDATARIO - SAS'),
        ('PRESIDENTE - SRL/SPA','PRESIDENTE - SRL/SPA'),
        ('TITOLARE - DITTA INDIVIDUALE','TITOLARE - DITTA INDIVIDUALE'),
        ('SOCIO - SNC','SOCIO - SNC'),
        ('AMMINISTRATORE DELEGATO - SRL/SPA','AMMINISTRATORE DELEGATO - SRL/SPA'),
        ('UFF. ACQUISTI','UFF. ACQUISTI'),
        ('UFF. TECNICO','UFF. TECNICO'),
        ('SEGRETARIA/O','SEGRETARIA/O'),
        ('RESP. LABORATORIO','RESP. LABORATORIO'),
        ('RESP. PRODUZIONE','RESP. PRODUZIONE'),
        ('UFF. FORNITORI','UFF. FORNITORI'),
        ('DIPENDENTE','DIPENDENTE'),
        ('LABORATORIO','LABORATORIO'),
        ('ARCHITETTO','ARCHITETTO'),
        ('RESP. INTERIOR','RESP. INTERIOR')
    ])

    fax = fields.Char('Fax')

    dipendenti = fields.Char('N° Dipendenti')
    partita_iva = fields.Char('Partita IVA')
    ateco = fields.Char('ATECO')

    settore = fields.Char('Settore')
    area_interesse = fields.Char("Area d'interesse")
    provincia = fields.Char('Provincia')
    region = fields.Char('Regione')
    sdi = fields.Char('Codice SDI')
    categoria_interesse = fields.Char('Categoria')
    codice_lavoro = fields.Char('Codice Lavoro')

    banca = fields.Char('Banca')
    iban = fields.Char('IBAN')

    fatturato = fields.Float('Ultimo fatturato')

    @api.model_create_multi
    def create(self, vals_list):
        if self.env.context.get('import_file'):
            self._check_import_consistency(vals_list)

        for vals in vals_list:
            vals['user_id'] = self.env.user.id
            vals['team_id'] = self.env.user.sale_team_id.id
            vals['company_id'] = self.env.user.company_id.id

            if vals.get('website'):
                vals['website'] = self._clean_website(vals['website'])
            if vals.get('parent_id'):
                vals['company_name'] = False

        partners = super(Partner, self).create(vals_list)

        if self.env.context.get('_partners_skip_fields_sync'):
            return partners

        for partner, vals in zip(partners, vals_list):
            partner._fields_sync(vals)
            partner._handle_first_contact_creation()

        return partners



    def create_opportunity(self):
        value = {
            'planned_revenue': False,
            'probability': 33.79,
            'name': self.name,
            'partner_id': self.id,
            'type': 'opportunity',
            'date_open': fields.Datetime.now(),
            'email_from': self.email,
            'phone': self.phone,
            'mobile': self.mobile,
            'date_conversion': fields.Datetime.now(),
            'user_id': self.env.user.id,
            'settore': self.settore,
            'provincia': self.provincia,
            'region': self.region,
            'area_interesse': self.area_interesse,
            'categoria_interesse': self.categoria_interesse,
            'ateco': self.ateco,
            'partita_iva': self.partita_iva,
            'codice_fiscale': self.l10n_it_codice_fiscale,
            'fatturato': self.fatturato
        }

        Lead = self.env['crm.lead'].create(value)

        return {
            'name': 'Opportunity',
            'view_mode': 'form',
            'res_model': 'crm.lead',
            'domain': [('type', '=', Lead.type)],
            'res_id': Lead.id,
            'view_id': False,
            'type': 'ir.actions.act_window',
            'context': {'default_type': Lead.type}
        }

    def get_directions(self):
        query = self.city+","+self.street+","+self.zip
        encoded_query = urllib.parse.quote(query)
        google_map_url = "https://www.google.com/maps/dir/?api=1&travelmode=driving&destination="+encoded_query

        return {
            'name'     : 'Go to website',
            'res_model': 'ir.actions.act_url',
            'type'     : 'ir.actions.act_url',
            'url'      : google_map_url
        }

    @api.onchange('name')
    def _onchange_name(self):
        existing_partner = self.env['res.partner'].search([('name','ilike',self.name)])
        if self.name and existing_partner:
            return {
                'warning': {
                    'title': 'Attenzione!',
                    'message': 'Esiste un contatto simile. Controllalo prima di continuare.'
                }
            }

    @api.onchange('relationship')
    def _onchange_relationship(self):
        if self.relationship == 'Cliente':
            self._origin.customer_rank = 1
            self._origin.supplier_rank = 0
        elif self.relationship == 'Fornitore':
            self._origin.customer_rank = 0
            self._origin.supplier_rank = 1
        else:
            self._origin.customer_rank = 0
            self._origin.supplier_rank = 0

    @api.onchange('provincia')
    def _onchange_provincia(self):
        provincia = self.env['res.country.state'].search([('code','=',self.provincia)])
        for p in provincia:
            if p.country_id.id == self.country_id.id:
                self.state_id = p.id
                break

    @api.onchange('email')
    def _onchange_email(self):
        for opportunity in self.opportunity_ids:
            opportunity.email_from = self.email
        
    @api.onchange('partita_iva')
    def _onchange_partita_iva(self):
        self._origin.vat = self.partita_iva



