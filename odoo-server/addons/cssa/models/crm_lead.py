# -*- coding: utf-8 -*-

from odoo import models, fields, api, tools
from datetime import date
import urllib.parse

import threading
import openerp
import openerp.release
import openerp.sql_db
import openerp.tools


class Lead(models.Model):
    _inherit = 'crm.lead'

    importo_preventivo = fields.Float('Importo preventivo')

    contact_type = fields.Selection([
        ('Cliente', 'Cliente'),
        ('Ex Cliente', 'Ex Cliente'),
        ('No', 'No'),
        ('Rischio', 'Rischio'),
        ('Non utilizzo', 'Non utilizzo'),
    ],'Tipologia Contatto')

    dipendenti = fields.Char('N° Dipendenti')
    fatturato = fields.Float('Fatturato')
    region = fields.Char('Regione')
    provincia = fields.Char('Provincia')
    settore = fields.Char('Settore')
    ateco = fields.Char("ATECO")
    area_interesse = fields.Char("Area d'interessse")
    categoria_interesse = fields.Char("Categoria d'interesse")

    posizione_lavorativa = fields.Selection([
        ('CEO - SRL/SPA','CEO - SRL/SPA'),
        ('AMMINISTRATORE UNICO - SRL/SPA','AMMINISTRATORE UNICO - SRL/SPA'),
        ('SOCIO ACCOMANDATARIO - SAS','SOCIO ACCOMANDATARIO - SAS'),
        ('PRESIDENTE - SRL/SPA','PRESIDENTE - SRL/SPA'),
        ('TITOLARE - DITTA INDIVIDUALE','TITOLARE - DITTA INDIVIDUALE'),
        ('SOCIO - SNC','SOCIO - SNC'),
        ('AMMINISTRATORE DELEGATO - SRL/SPA','AMMINISTRATORE DELEGATO - SRL/SPA'),
        ('UFF. ACQUISTI','UFF. ACQUISTI'),
        ('UFF. TECNICO','UFF. TECNICO'),
        ('SEGRETARIA/O','SEGRETARIA/O'),
        ('RESP. LABORATORIO','RESP. LABORATORIO'),
        ('RESP. PRODUZIONE','RESP. PRODUZIONE'),
        ('UFF. FORNITORI','UFF. FORNITORI'),
        ('DIPENDENTE','DIPENDENTE'),
        ('LABORATORIO','LABORATORIO'),
        ('ARCHITETTO','ARCHITETTO'),
        ('RESP. INTERIOR','RESP. INTERIOR')
    ])

    forma_societaria = fields.Selection([
        ('Ditta Individuale','Ditta Individuale'),
        ('SNC','SNC'),
        ('SAS','SAS'),
        ('SRL','SRL'),
        ('SRLS','SRLS'),
        ('SPA','SPA')

    ], 'Forma Societaria')

    partita_iva = fields.Char('Partita IVA')
    codice_fiscale = fields.Char('Codice Fiscale')

    activity_orario = fields.Char('Orario')
    orario = fields.Char('Orario')

    now = fields.Char()

    @api.model
    def create(self, vals):
        context = dict(self._context or {})
        if vals.get('provincia'):
            context['provincia'] = vals.get('provincia')
            self.partner_id.provincia = vals.get('provincia')
        if vals.get('region'):
            context['region'] = vals.get('region')
            self.partner_id.region = vals.get('region')
        if vals.get('partita_iva'):
            context['vat'] = vals.get('partita_iva')
            self.partner_id.partita_iva = vals.get('partita_iva')
            self.partner_id.vat = vals.get('partita_iva')
        if vals.get('settore'):
            context['settore'] = vals.get('settore')
            self.partner_id.settore = vals.get('settore')
        if vals.get('ateco'):
            context['ateco'] = vals.get('ateco')
            self.partner_id.ateco = vals.get('ateco')
        if vals.get('area_interesse'):
            context['area_interesse'] = vals.get('area_interesse')
            self.partner_id.area_interesse = vals.get('area_interesse')
        if vals.get('categoria_interesse'):
            context['categoria_interesse'] = vals.get('categoria_interesse')
            self.partner_id.categoria_interesse = vals.get('categoria_interesse')
        if vals.get('dipendenti'):
            context['dipendenti'] = vals.get('dipendenti')
            self.partner_id.dipendenti = vals.get('dipendenti')
        if vals.get('fatturato'):
            context['fatturato'] = vals.get('fatturato')
            self.partner_id.fatturato = vals.get('fatturato')

        lead = super(Lead, self).create(vals)

        return lead

    def write(self, vals):
        if vals.get('website'):
            vals['website'] = self.env['res.partner']._clean_website(vals['website'])

        if vals.get('region'):
            self.partner_id.region = vals.get('region')
        if vals.get('provincia'):
            self.partner_id.provincia = vals.get('provincia')
        if vals.get('settore'):
            self.partner_id.settore = vals.get('settore')
        if vals.get('ateco'):
            self.partner_id.ateco = vals.get('ateco')
        if vals.get('area_interesse'):
            self.partner_id.area_interesse = vals.get('area_interesse')
        if vals.get('categoria_interesse'):
            self.partner_id.categoria_interesse = vals.get('categoria_interesse')
        if vals.get('fatturato'):
            self.partner_id.fatturato = vals.get('fatturato')
        if vals.get('n_dipendenti'):
            self.partner_id.n_dipendenti = vals.get('n_dipendenti')
        if vals.get('partita_iva'):
            self.partner_id.partita_iva = "IT"+vals.get('partita_iva')
            self.partner_id.vat = "IT"+vals.get('partita_iva')
        if vals.get('codice_fiscale'):
            self.partner_id.l10n_it_codice_fiscale = vals.get('codice_fiscale')
        if vals.get('email_from'):
            self.partner_id.email = vals.get('email_from')
        if vals.get('phone'):
            self.partner_id.phone = vals.get('phone')
        if vals.get('name'):
            self.partner_id.name = vals.get('name').upper()
        if vals.get('forma_societaria'):
            if vals.get('forma_societaria') not in self.partner_id.name:
                self.partner_id.name = self.partner_id.name + " " + vals.get('forma_societaria')

        # stage change: update date_last_stage_update
        if 'stage_id' in vals:
            vals['date_last_stage_update'] = fields.Datetime.now()
            stage_id = self.env['crm.stage'].browse(vals['stage_id'])

            if stage_id.is_won:
                vals.update({'probability': 100})

        # Only write the 'date_open' if no salesperson was assigned.
        if vals.get('user_id') and 'date_open' not in vals and not self.mapped('user_id'):
            vals['date_open'] = fields.Datetime.now()
        # stage change with new stage: update probability and date_closed
        if vals.get('probability', 0) >= 100 or not vals.get('active', True):
            vals['date_closed'] = fields.Datetime.now()
        elif 'probability' in vals:
            vals['date_closed'] = False
        if vals.get('user_id') and 'date_open' not in vals:
            vals['date_open'] = fields.Datetime.now()

        write_result = super(Lead, self).write(vals)
        # Compute new automated_probability (and, eventually, probability) for each lead separately
        if self._should_update_probability(vals):
            self._update_probability()

        return write_result


    def get_directions(self):
        query = self.partner_id.city+","+self.partner_id.street+","+self.partner_id.zip
        encoded_query = urllib.parse.quote(query)
        google_map_url = "https://www.google.com/maps/dir/?api=1&travelmode=driving&destination="+encoded_query

        return {
            'name'     : 'Go to website',
            'res_model': 'ir.actions.act_url',
            'type'     : 'ir.actions.act_url',
            'url'      : google_map_url
       }


    def register_skype_activity(self):
        skip_activity = False

        for act in self.activity_ids:
            if act.activity_type_id.id == 2 and act.state == 'overdue' and act.state == 'today' and act.state == 'planned':
                skip_activity = True
                break

        if not skip_activity:
            activity = self.env['mail.activity']
            activity.create({
                'res_id': self.id,
                'res_model_id': 199,
                'res_model': 'crm.lead',
                'activity_type_id': 2,
                'date_deadline': date.today(),
                'activity_category': 'default',
                'previous_activity_type_id': False,
                'recommended_activity_type_id': False,
                'user_id': self.user_id.id
            }).action_feedback()

    def action_call_telephone(self):
        if self.phone or self.partner_id.phone:
            self.register_skype_activity()
            if self.partner_id.phone:
                skype_number = 'skype:' + self.partner_id.phone + '?call'
            else:
                skype_number = 'skype:' + self.phone + '?call'
            return {
                'type': 'ir.actions.act_url',
                'url': skype_number,
                'target': 'new',
            }

    def _onchange_partner_id_values(self, partner_id):
        """ returns the new values when partner_id has changed """
        if partner_id:
            partner = self.env['res.partner'].browse(partner_id)

            partner_name = partner.parent_id.name
            if not partner_name and partner.is_company:
                partner_name = partner.name

            contact_name = False
            contact_title = ''
            contact_function = ''
            if partner.is_company == False:
                contact_name = partner.name
                contact_title = partner.title.id
                contact_function = partner.posizione_lavorativa
            elif len(partner.child_ids) > 0:
                contact_name = partner.child_ids[0].name
                contact_title = partner.child_ids[0].title.id
                contact_function = partner.child_ids[0].posizione_lavorativa

            return {
                'partner_name': partner_name,
                'contact_name': contact_name,
                'title': contact_title,
                'street': partner.street,
                'street2': partner.street2,
                'city': partner.city,
                'state_id': partner.state_id.id,
                'country_id': partner.country_id.id,
                'email_from': partner.email,
                'phone': partner.phone,
                'mobile': partner.mobile,
                'zip': partner.zip,
                'function': partner.function,
                'posizione_lavorativa': contact_function,
                'website': partner.website,
                'dipendenti': partner.dipendenti
            }
        return {}

    def _create_lead_partner_data(self, name, is_company, parent_id=False):
        """ extract data from lead to create a partner
            :param name : furtur name of the partner
            :param is_company : True if the partner is a company
            :param parent_id : id of the parent partner (False if no parent)
            :returns res.partner record
        """
        email_split = tools.email_split(self.email_from)
        res = {
            'name': name,
            'user_id': self.env.context.get('default_user_id') or self.user_id.id,
            'comment': self.description,
            'team_id': self.team_id.id,
            'parent_id': parent_id,
            'phone': self.phone,
            'mobile': self.mobile,
            'email': email_split[0] if email_split else False,
            'title': self.title.id,
            'street': self.street,
            'street2': self.street2,
            'zip': self.zip,
            'city': self.city,
            'country_id': self.country_id.id,
            'state_id': self.state_id.id,
            'website': self.website,
            'is_company': is_company,
            'type': 'contact',
            'provincia': self.provincia,
            'region': self.region,
            'vat': self.partita_iva,
            'partita_iva': self.partita_iva,
            'settore': self.settore,
            'area_interesse': self.area_interesse,
            'ateco': self.ateco,
            'categoria_interesse': self.categoria_interesse,
            'dipendenti': self.dipendenti,
            'fatturato': self.fatturato,
            'function': self.posizione_lavorativa
        }
        if self.lang_id:
            res['lang'] = self.lang_id.code
        return res

    def action_assign_new_opportunity(self, stage_id):
        value = {
            'planned_revenue': self.planned_revenue,
            'name': self.name,
            'partner_id': self.partner_id.id,
            'type': 'opportunity',
            'date_open': fields.Datetime.now(),
            'email_from': self.partner_id and self.partner_id.email or self.email_from,
            'phone': self.partner_id and self.partner_id.phone or self.phone,
            'date_conversion': fields.Datetime.now(),
            'stage_id': stage_id,
            'activity_ids': None,
            'message_ids': None,
            'settore': self.settore,
            'region': self.region,
            'provincia': self.provincia,
            'ateco': self.ateco,
            'area_interesse': self.area_interesse,
            'categoria_interesse': self.categoria_interesse
        }

        value['fatturato'] = self.fatturato
        value['dipendenti'] = self.dipendenti

        value['partita_iva'] = self.partita_iva
        value['codice_fiscale'] = self.codice_fiscale
        value['fatturato'] = self.fatturato

        value['forma_societaria'] = self.forma_societaria
        value['contact_type'] = self.contact_type

        return self.env['crm.lead'].create(value)

    def action_translate(self):
        thread = threading.Thread(target=self.translate, args=())
        thread.daemon = True                            # Daemonize thread
        thread.start()

    def translate(self):
        with api.Environment.manage():
            with openerp.registry(self.env.cr.dbname).cursor() as new_cr:
                new_env = api.Environment(new_cr, self.env.uid, self.env.context)
                translations = new_env['ir.translation'].search()

                for translation in translations:
                    try:
                        if 'odoo' in translation.value:
                            translation.value = (translation.value).replace('odoo','cssa')
                        if 'Odoo' in translation.value:
                            translation.value = (translation.value).replace('Odoo','Cssa')
                        if 'odoo' in translation.src:
                            translation.src = (translation.src).replace('odoo','cssa')
                        if 'Odoo' in translation.src:
                            translation.src = (translation.src).replace('Odoo','Cssa')
                    except:
                        print('Error:',translation.src)
