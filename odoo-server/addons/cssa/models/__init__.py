# -*- coding: utf-8 -*-

from . import res_partner
from . import crm_lead
from . import calendar
from . import cssa
from . import mail_activity
from . import crm_activity_report