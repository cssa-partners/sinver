# -*- coding: utf-8 -*-
# Copyright 2019 GTICA.C.A. - Ing Henry Vivas

{
    'name': 'Whatsapp + CRM Integration',
    'summary': 'Integration Whatsapp and CRM',
    'version': '13.0.3.0.0',
    'category': 'CRM',
    'author': 'GTICA.C.A',
    'support': 'controlwebmanager@gmail.com',
    'license': 'OPL-1',
    'website': 'https://gtica.online/',
    'price': 21.00,
    'currency': 'EUR',
    'depends': [
        'gtica_whatsapp_template',
        'crm',
    ],
    'data': [
        'views/view_integration_crm.xml',
        'wizard/wizard_whatsapp_integration_crm.xml'
    ],
    'images': ['static/description/main_screenshot.png'],
    'application': False,
    'installable': True,
}
